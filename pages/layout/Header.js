import { Navbar, Nav } from 'react-bootstrap'

export default () => (
  <div style={{
    marginBottom: '5%'
  }}>
    <Navbar fixed="top" collapseOnSelect expand="lg" bg="dark" variant="dark">
      <Navbar.Toggle aria-controls="responsive-navbar-nav" />
      <Navbar.Brand href="/">chanoch.com</Navbar.Brand>
      <Navbar.Collapse >
        <Nav>
          <Nav.Link accessKey="k" href="/stories">
            Stories
          </Nav.Link>
        </Nav>
      </Navbar.Collapse>
    </Navbar>

  </div>
)